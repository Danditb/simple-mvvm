package com.example.simple_mvvm.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class People implements Serializable {
    @SerializedName("gender")
    public String gender;
    @SerializedName("name")
    public String name;
    @SerializedName("location")
    public String location;
    @SerializedName("email")
    public String mail;
    @SerializedName("login")
    public String login;
    @SerializedName("phone")
    public String phone;
    @SerializedName("cell")
    public String cell;
    @SerializedName("picture")
    public String picture;

    public String fullname;

    public boolean hasEmail(){
        return mail !=null && !mail.isEmpty();
    }
}
